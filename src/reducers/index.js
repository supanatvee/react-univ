"use strict"
import {
  combineReducers
} from 'redux';

// HERE IMPORT REDUCERS TO BE COMBINED
import {
  booksReducers
} from './booksReducers';
import {
  cartReducers
} from './cartReducers';
import {
  memberReducers
} from './memberReducers';

//HERE COMBINE THE REDUCERS
export default combineReducers({
  books: booksReducers,
  cart: cartReducers,
  member: memberReducers
})
