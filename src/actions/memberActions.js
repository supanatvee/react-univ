"use strict"
import axios from 'axios';
// CREATE
export function createMember(_username, _password, _display, _name, _surname, _email, _phone) {
  return function(dispatch) {
    // TODO : Add [display, email, username] duplicate detector
    axios.post('/api/member', {
      image: '',
      username: _username,
      password: _password,
      display: _display,
      name: _name,
      surname: _surname,
      email: _email,
      phone: _phone,
      priority: "view",
      status: "pending"
    }).then(function(response) {
      dispatch({
        type: "CREATE_MEMBER",
        payload: response.data
      })
    }).catch(function(err) {
      dispatch({
        type: "CREATE_MEMBER_REJECTED",
        payload: err
      })
    })
  }
}
// GET MEMBER SESSION
export function getMemberSession() {
  return function(dispatch) {
    axios.get('/api/member/signIn')
      .then(function(response) {
        dispatch({
          type: "GET_SESSION_MEMBER",
          payload: response.data
        })
      }).catch(function(err) {
        dispatch({
          type: "GET_SESSION_MEMBER_REJECTED",
          payload: err
        })
      })
  }
}
// SIGN IN
export function signIn(_username, _password) {
  return function(dispatch) {
    axios.post('/api/member/signIn', {
        username: _username,
        password: _password
      })
      .then(function(response) {
        dispatch({
          type: "STORE_MEMBER",
          payload: response.data
        })
      }).catch(function(err) {
        dispatch({
          type: "STORE_MEMBER_REJECTED",
          payload: err
        })
      })
  }
}
// DESTROY MEMBER SESSION
export function destroyMemberSession() {
  return function(dispatch) {
    axios.delete('/api/member/signIn')
      .then(function(response) {
        dispatch({
          type: "DESTROY_SESSION_MEMBER",
          payload: response.data
        })
      }).catch(function(err) {
        dispatch({
          type: "DESTROY_SESSION_MEMBER_REJECTED",
          payload: err
        })
      })
  }
}
// SET PRIORITY
export function changePriorityTo(_username, _password, _priority) {
  return function(dispatch) {
    axios.put('/api/member/update/' + _username, {
      priority: _priority
    }).then(function(response) {
      dispatch({
        type: "CHANGE_PRIORITY_MEMBER",
        payload: response.data
      })
    }).catch(function(err) {
      dispatch({
        type: "CHANGE_PRIORITY_MEMBER_REJECTED",
        payload: err
      })
    })
  }
}
// SET STATUS
export function changeStatusTo(_username, _password, _status) {
  return function(dispatch) {
    axios.put('/api/member/update/' + _username, {
      status: _status
    }).then(function(response) {
      dispatch({
        type: "CHANGE_STATUS_MEMBER",
        payload: response.data
      })
    }).catch(function(err) {
      dispatch({
        type: "CHANGE_STATUS_MEMBER_REJECTED",
        payload: err
      })
    })
  }
}
